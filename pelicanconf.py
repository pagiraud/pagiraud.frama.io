#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Pierre-Amiel Giraud'
SITENAME = 'Insolit'
SITESUBTITLE = 'Informatique, logiciels libres et territoires'
SITEURL = ''
SITELOGO = SITEURL + '/images/profil.jpg'

PLUGIN_PATHS = ['/data/pelican-plugins']
PLUGINS = ['i18n_subsites']
JINJA_ENVIRONMENT = {'extensions': {'jinja2.ext.i18n'}}

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Athens'

#DEFAULT_LANG = u'en'

STATIC_PATHS = ['images', 'audio', 'pdf', 'autres', 'oral']

ARTICLE_EXCLUDES = ['oral']

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (#('Plugins de recherche pour les collections Isidore', 
#'http://www.insolit.org/isidore-opensearch/'),
       #  ('Python.org', 'http://python.org/'),
         ('Doc\'Géo', 'https://docgeo.hypotheses.org/'),
         ('ORCID', 'https://orcid.org/0000-0002-7977-4273'),)

# Social widget
SOCIAL = (('gitlab', 'https://framagit.org/pagiraud'),
          ('mastodon', 'https://toot.aquilenet.fr/@insolit'),
          ('twitter', 'http://twitter.com/insolit_blog'),
          ['rss', '/feeds/all.atom.xml'])

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'Flex'

#Options pour Flex
SITETITLE = 'Insolit'
MAIN_MENU = True

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'version': '4.0',
    'slug': 'by-sa'
}

MENUITEMS = (('Catégories', 'categories.html'),
              ('Archives', 'archives.html'),
              ('Étiquettes', 'tags.html'))

DISQUS_SITENAME = 'insolit'

# Default theme language.
I18N_TEMPLATES_LANG = 'en'
# Your language.
DEFAULT_LANG = 'fr'
OG_LOCALE = 'fr_FR'
LOCALE = ('fr_FR')
