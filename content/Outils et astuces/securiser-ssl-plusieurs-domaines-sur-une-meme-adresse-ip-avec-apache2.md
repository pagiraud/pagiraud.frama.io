Title: Sécuriser (SSL) plusieurs domaines sur une même adresse IP avec Apache2
Date: 2012-07-23 8:00
Tags: Sécurité, Administration de serveur, SSL

Il s’agit d’une opération somme toute assez triviale, mais les informations que l’on peut trouver ici ou là à ce sujet sont assez contradictoires voire erronées, y compris sur le propre site d’Apache. À partir d’éléments de réponse éparpillés sur le web, j’ai pu me débrouiller. Pour vous éviter d’avoir à faire plusieurs essais et à trier le bon grain de l’ivraie, j’ai décidé de rédiger ce tutoriel qui vous guidera pas à pas, depuis l’activation du SSL sous Apache jusqu’à la configuration des VirtualHosts.  

# L’expression du besoin

Tout d’abord, il s’agit de savoir ce que l’on entend par plusieurs domaines sur une même adresse IP. Ici, il faut entendre domaine au sens de [FQDN](https://fr.wikipedia.org/wiki/Fully_qualified_domain_name "Fully Qualified Domain Name") sans point final. Vous êtes concerné par exemple si vous souhaitez sécuriser deux sites avec deux noms de domaines différents hébergés sur la même machine, par exemple pour moi&#8239;:

*   `www.insolit.org`

*   `www.saintseiya.tv`

Vous l’êtes également si vous souhaitez sécuriser plusieurs sous-domaines également hébergés sur la même machine, par exemple&#8239;:

*   `www.saintseiya.tv`

*   `forum.saintseiya.tv`

Vous pourriez très bien vous contenter de sécuriser le sous-domaine www en rendant accessible le forum via www.saintseiya.tv/forum, mais la solution est moins esthétique. De plus, procéder par sous-domaine rajoute un soupçon de sécurité à votre site. Regardez par exemple vos logs Apache, vous comprendrez.

# Vérification de l’installation

Pour réaliser les opérations qui suivent, votre installation doit supporter les <acronym title="Server Name Indication">SNI</acronym>. Autrement dit, vous devez disposer d’au moins la version 0.9.8 d’OpenSSL et 2.2.12 d’Apache. Comme ces deux versions datent respectivement de 2007 et 2009, cela ne devrait poser aucun problème sur des distributions récentes.

En principe, tous les paquets nécessaires sont installés par défaut sur les principales distributions de serveurs GNU/Linux. Au pire, un petit coup d’apt-get, d’aptitude, de yum ou autre et le tour est joué. 😉

# Obtenir les certificats&#8239;: le plat de résistance

Il y a bien sûr la méthode simple (auto-signature), qui permet en une commande d’obtenir un certificat. Si l’utilisation que vous faites de votre serveur est purement privée, vous pouvez vous en contenter&#8239;:

	sudo make-ssl-cert /usr/share/ssl-cert/ssleay.cnf /etc/ssl/private/localhost.pem

Cependant, même dans le cas d’une utilisation privée, cela ne me semble pas optimal. L’appel à une autorité extérieure me paraît préférable (et en plus ça apprend les bonnes manières pour un usage public). Il existe plusieurs solutions gratuites de certification. Si vous avez un domaine chez [Gandi](http://www.gandi.net "Un registrar bien connu"), vous avez droit à un certificat gratuit par exemple <del>(que j’ai gaspillé en faisant n’importe quoi)</del>. Il semblerait que [StartSSL](http://startssl.com/ "StartSSL, une solution gratuite de certification SSL") fournisse également ce genre de service.

Pour ma part, j’ai opté pour [CAcert](http://www.cacert.org "CAcert"), une autorité de certification gratuite et associative. Les certificats qu’elle fournit produisent une erreur de sécurité (au moins avec Firefox), il vous faudra donc confirmer une exception permanente. CACert est donc à éviter si vous souhaitez faire un usage public de votre site sécurisé, sauf si vous avez à faire à un public web savvy, tel [DLFP](https://linuxfr.org "Da Linux French Page").

Bref, procédons.

## Quelques préliminaires&#8239;: inscription et vérification du nom de domaine

Inscrivez-vous sur CAcert (il suffit de suivre les instructions), puis dans le menu de droite allez sur **Domains > Add**. Entrez le nom de domaine que vous souhaitez sécuriser, sans le sous-domaine. Dans mon cas ce fut&#8239;:

	insolit.org

Sur l’écran suivant, vous devez choisir une adresse email vers laquelle envoyer une vérification dans une liste restreinte avec des choix du type webmaster@mondomaine.tld . Si vous ne possédez aucune des adresses référencées, votre registrar vous fournit sûrement un service de redirection gratuit qu’il vous suffit d’activer. C’est le cas chez Gandi en tout cas. Suivez ensuite le reste des instructions.

## Génération de la demande de signature de certificat sur votre serveur

Nous allons à présent générer une demande de signature de certificat, ou <acronym title="Certificate Signing Request">CSR</acronym>, grâce à OpenSSL.

	openssl req -newkey rsa:2048 -subj /CN=*.domaine.tld -nodes -keyout domaine_cle_privee.pem -out domaine_csr.pem

Remplacez *.domaine.tld par votre domaine, par exemple *.insolit.org Vous pouvez choisir de préciser le sous-domaine (www par exemple), mais cela signifiera que vous devrez faire autant de certificats que de sous-domaines à sécuriser. Vous pouvez aussi modifier le nom des fichiers de sortie à votre convenance.

Ouvrez votre CSR et copiez TOUT son contenu.

## Installation du certificat généré par CAcert

Reconnectez-vous sur CAcert. Dans le panneau de droite, rendez-vous sur Server Certificates > New. Collez l’intégralité de votre CSR dans la boîte de texte. Vérifiez bien que le contenu de la boîte commence par `-----BEGIN CERTIFICATE REQUEST-----` et se termine par `-----END CERTIFICATE REQUEST-----`. Cliquez sur Generate. Si vous avez bien suivi mes instructions, CAcert devrait vous retourner un «&#8239;joli&#8239;» certificat. Copiez-le et enregistrez-le sur votre serveur, sous le nom domaine_cert.pem par exemple.

Il s’agit maintenant de déplacer les fichiers dans un répertoire approprié. Les commandes suivantes partent du principe que vous n’avez pas modifié les noms de fichiers proposés plus haut.

	sudo mv domaine_cert.pem /etc/ssl/certs/ && sudo mv domaine_cle_privee.pem /etc/ssl/priv/

Vous pouvez conserver votre CSR si ça vous amuse, mais il ne vous servira désormais plus à rien. Un petit coup de rm devrait lui régler son compte.

# Configuration d’Apache

## Mise en place des hôtes virtuels basés sur des noms

Ils sont plus couramment appelés par leur nom anglais, name-based virtual hosts. On va commencer, si ce n’est déjà fait, par activer le module SSL d’Apache&#8239;:

	sudo a2enmod ssl

Puis, ouvrez le fichier _/etc/apache2/ports.conf_ et ajoutez la ligne

	NameVirtualHost *:443

juste après celle quasi identique portant sur le port 80\. Assurez-vous également que la directive _Listen 443_ n’est pas commentée. vous devriez donc avoir, au minimum&#8239;:

	NameVirtualHost *:80
	NameVirtualHost *:443

	Listen 80

	<IfModule mod_ssl.c>
		Listen 443
	</IfModule>

## Créer les fichiers de configuration des hôtes virtuels

Il s’agit simplement de copier un hôte virtuel fonctionnel en accès non sécurisé (de type http) et de le modifier en ajoutant les lignes spécifiques au SSL.

On commence donc par copier le fichier de configuration&#8239;:

	sudo cp /etc/apache2/sites-available/{domaine,domaine_ssl}

Ouvrez ensuite le fichier domaine_ssl. L’une des premières lignes devrait être `<VirtualHost *:80>`. Remplacez le 80 par 443. Rajoutez une ligne pour activer le SSL après la déclaration du ServerName&#8239;:

	SSLEngine On

À la suite, indiquez la localisation de votre clé privée et de votre certificat&#8239;:

	SSLCertificateFile /etc/ssl/certs/domaine_cert.pem
	SSLCertificateKeyFile /ssl/certs/priv/domaine_cle_privee.pem

Et c’est tout&#8239;! Recommencez cette dernière opération pour tous les sous-domaines nécessaires. Si vous possédez plusieurs noms de domaines sur la même machine, pensez bien à générer un certificat par domaine. On active l’hôte virtuel et on teste&#8239;:

	sudo a2ensite domaine_ssl
	/etc/init.d/apache2 reload

# Conclusion et crédits

Normalement, tout devrait fonctionner. Si ce n’est pas le cas, pensez à consulter les logs d’Apache pour en savoir plus.

J’ai réalisé ce tutoriel en en croisant 3 partiels / incomplets&#8239;:

1.  [Sécuriser Apache2 avec SSL](http://doc.ubuntu-fr.org/tutoriel/securiser_apache2_avec_ssl "Sécuriser Apache2 avec SSL"), du wiki Ubuntu-fr&#8239;;
2.  [Simple Apache Cert](http://wiki.cacert.org/SimpleApacheCert "Simple Apache Cert"), du wiki de CACert&#8239;;
3.  [Name Based Virtual Hosts with SSL using Apache2 on Ubuntu Lucid](http://www.codealpha.net/631/name-based-virtual-hosts-with-ssl-using-apache2-on-ubuntu-lucid/ "Billet du blog code(alpha)"), sur le blog code(alpha).
