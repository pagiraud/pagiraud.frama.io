Title: Se débarasser d'un paragraphe indésirable sous un tableau (LibreOffice)
Date: 2011-10-24 8:00
Tags: LibreOffice, raccourci
Slug: se-debarasser-dun-paragraphe-indesirable-sous-un-tableau-libreoffice
# Pour les gens très pressés

Après le dernier caractère de la cellule en bas à droite, presser **Ctrl+Maj+Suppr**.

# Mode verbeux

Dans LibreOffice (et OpenOffice.org), un tableau ne peut être suivi que par un paragraphe. La plupart du temps, ce comportement est très pratique, car il permet de continuer facilement à insérer du texte. Néanmoins, dans certains cas, cela peut s’avérer gênant. Par exemple, si votre document se termine par un tableau et que l’ultime saut de ligne force l’inclusion d’une page supplémentaire. Ou encore, si vous souhaitez insérer un tableau dans l’en-tête ou le pied de page.  
Comme souvent, un petit tour sur le [bugtracker](https://secure.wikimedia.org/wikipedia/en/wiki/Bug_tracking_system "Bug tracking system") du logiciel donne [la solution](https://bugs.freedesktop.org/show_bug.cgi?id=33631 "Bug 33631 - Can't remove new line after table"), et comme souvent aussi, _«&#8239;[it’s not a bug, it’s a feature](https://secure.wikimedia.org/wiktionary/fr/wiki/it%E2%80%99s_not_a_bug,_it%E2%80%99s_a_feature "Bug or feature?")&#8239;»_. L’ajout par défaut du paragraphe est défendu par les développeurs pour les raisons évoquées plus haut, et de toute façon un raccourci existe. Il faut se placer après le dernier caractère de la cellule en bas à droite du tableau (dernière ligne, dernière colonne) et presser simultanément&#8239;: **Ctrl+Maj+Suppr**.  
Que les choses soient claires, il est très probable que ce raccourci fonctionne également sous <del>Sun</del> <del datetime="2011-10-24T12:34:09+00:00">Oracle</del> Apache OpenOffice.org, mais je n’ai pas vérifié.

