Title: Créer des raccourcis clavier pour l’extension Zotero de LibreOffice
Date: 2013-02-23 8:00
Tags: LibreOffice, Zotero
Slug: creer-des-raccourcis-claviers-pour-lextension-zotero-de-libreoffice
Vous connaissez sûrement déjà [Zotero](https://www.zotero.org/ "Extension Zotero pour Firefox"), une extension pour Firefox de gestion de bibliographie. L’outil existe d’ailleurs en version _standalone_, ce qui permet de le faire fonctionner aussi avec Safari ou Chrome/Chromium. C’est un logiciel très puissant, pour lequel il existe [ça](http://www.boiteaoutils.info/2012/06/introduction-zotero-30-nouveau-tutoriel.html "Tutoriel pour Zotero sur la boîte à outils des historiens") et [là](http://www.bib.umontreal.ca/lgb/Zotero/default.htm "Tutoriel pour Zotero sur le site de l'université de Montréal") de très bons tutoriels.

Il existe également des [extensions pour différents traitements de texte](https://www.zotero.org/support/word_processor_plugin_installation "Installing Word Processor Plugins for Zotero 3.0 for Firefox") connus, dont LibreOffice, OpenOffice et NeoOffice. Elles permettent d’appeler depuis le traitement de texte, via une interface très bien pensée, des références bibliographiques stockées dans Zotero. Et heureusement, car l’outil de gestion de bibliographie intégré à LibreOffice n’est pas des plus pratiques&#8239;!

Cet article s’adresse plus particulièrement à ceux et celles qui ont déjà installé Zotero et l’extension pour LibreOffice. Normalement, ce que je décris devrait fonctionner aussi sous OpenOffice et NeoOffice <del>mais je n’ai pas vérifié</del>, ce qui a gentiment été vérifié par Anne Guégan.

Par défaut, aucun raccourci clavier n’est attribué aux fonctionnalités de cette extension. Il faut donc utiliser la souris ou le _touchpad_ pour demander l’insertion d’une référence ou de la bibliographie. En aficionado du tout clavier, j’ai donc cherché comment ajouter les raccourcis en question. Ce n’est pas difficile pour deux sous, mais pas intuitif non plus&#8239;!

D’abord, sélectionnez le menu **Outils > Personnaliser…** Ensuite, sélectionnez l’onglet **Clavier**.

![zotero libreoffice]({static}/images/zotero-libreoffice.png "Fenêtre de modification des raccourcis claviers de LibreOffice")

Une fenêtre semblable à celle de droite apparaît. C’est la fenêtre classique pour changer les raccourcis clavier de LibreOffice. Il ne reste alors plus que 4 étapes (mais il faut réitérer les trois dernières pour chaque raccourci)&#8239;:

1.  Dans le menu **Catégorie**, descendez l’ascenseur jusqu’en bas. Développez la liste **Macros LibreOffice > user > Zotero** et sélectionnez **Zotero**.
2.  Sélectionnez une **Fonction** pour laquelle vous souhaitez créer un raccourci. AMHA, les deux plus utiles sont ZoteroAddCitation et ZoteroEditCitation.
3.  Choisissez le **Raccourci clavier** que vous souhaitez affecter à cette fonction, en prenant garde de ne pas en prendre un déjà utilisé par une fonction intéressante. Je pense que pour les deux fonctions déjà évoquées CTRL+MAJ+C (pour Citation) et CTRL+MAJ+E (pour Édition) sont de bons choix.
4.  Validez votre choix en cliquant sur Modifier. Recommencez les étapes 2 à 4 pour chaque fonction pour laquelle vous souhaitez créer un raccourci.

C’est bien pratique en mode plein écran&#8239;!
