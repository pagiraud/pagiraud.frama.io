Title: Rédiger un plugin de recherche intégré au navigateur
Date: 2013-03-31 8:00
Tags: firefox, ISIDORE, Mycroft, plugins, REGARDS
Slug: rediger-un-plugin-de-recherche-integre-au-navigateur
Voici un petit tutoriel qui m’a été demandé à la suite des [plugins que j’ai rédigés](http://mycroftproject.com/search-engines.html?name=regards "Plugins de recherche pour le centre REGARDS"), et qui permettent de savoir quels sont les documents dont dispose le centre [REGARDS](http://www.regards.cnrs.fr/ "Le centre de documentations REGARDS").

**Objectif&#8239;:** rédiger un plugin de recherche intégré au navigateur interrogeant la plate-forme [ISIDORE](http://www.rechercheisidore.fr/ "La plate-forme ISIDORE") et ne retournant que les résultats d’une collection ou organisation spécifique.

**Exemple d’utilisation&#8239;:** Votre centre de documentation est moissonné par ISIDORE et vous souhaitez ne plus avoir à maintenir votre moteur de recherche maison. Cependant, vos utilisateurs souhaitent savoir facilement à quels documents ils peuvent accéder physiquement.

**Avantages&#8239;:** Cette solution est très simple à mettre en œuvre. Elle ne requiert aucune modification sur le site de l’organisation ou de la collection visée (même si cela reste conseillé) ni aucune maintenance (sauf changement dans la structuration des requêtes de la plateforme ISIDORE).

**Limites&#8239;:** Seuls les résultats d’une collection sont retournés. Si l’objectif est de prioriser ceux d’une collection, faisant apparaître les autres documents à la suite, il faut réaliser une interface en utilisant l’API d’ISIDORE.

Avant de rentrer dans le vif du sujet, je vous propose un petit topo sur l’ajout et la gestion de moteurs de recherche dans les navigateurs les plus courants. Ceux qui connaissent déjà le sujet peuvent sauter cette première partie sans problème.

# Les plugins de recherche intégrés aux navigateurs

[OpenSearch](http://fr.wikipedia.org/wiki/OpenSearch "Article Wikipédia sur OpenSearch") est un ensemble cohérent de techniques permettant d’exécuter des recherches sur un site sans passer par une interface web. OpenSearch est supporté par les navigateurs suivants&#8239;:

*   Mozilla Firefox 2 et versions ultérieures&#8239;;
*   Windows Internet Explorer 7 et versions ultérieures&#8239;;
*   Google Chrome / Google Chromium&#8239;;
*   Apple Safari 5.1 et versions ultérieures, moyennant l’installation d’[une extension](http://www.opensearchforsafari.com/ "Extension OpenSearch pour Apple Safari").

![Les moteurs de la barre de recherche de Firefox]({static}/images/firefox_opensearch.png)

Prenons le cas de Mozilla Firefox. OpenSearch y est exploité dans la [barre de recherche](https://support.mozilla.org/fr/kb/barre-recherche-choisir-facilement-moteur-favori#os=win7&browser=fx19 "Barre de recherche Mozilla Firefox"). Cela permet justement de faire une recherche, par exemple dans un dictionnaire en ligne, sans avoir d’abord à aller sur leur site. Une bonne pratique est d’attribuer à chacun de vos moteurs un [mot clé](https://support.mozilla.org/fr/kb/barre-recherche-choisir-facilement-moteur-favori#w_mots-claes "Attribuer un mot clé à un moteur de recherche"). Ainsi, vous pouvez passer d’un moteur à l’autre sans même toucher la souris.

Par exemple, vous attribuez le mot clé **d** au moteur «&#8239;Portail lexical – CNRTL&#8239;» (le dictionnaire en ligne du CNRS). En tapant **Ctrl + l**, votre curseur se positionne dans la barre d’adresse. Tapez ensuite le mot clé de votre moteur (ici **d**) suivi d’un espace et de la recherche que vous souhaitez effectuer. C’est simple et très efficace.

Les autres navigateurs proposent des fonctions plus ou mois similaires. À noter que seule une solution basée sur des raccourcis claviers est disponible dans Google Chrome, une seule barre servant à tout.

Quel que soit votre navigateur, il y a deux façons d’ajouter un plugin de recherche. Le site officiel de Mozilla fournit [de très bonnes explications](https://support.mozilla.org/fr/kb/barre-recherche-choisir-facilement-moteur-favori#w_ajouter-des-moteurs-de-recherche "Ajouter des moteurs de recherche à Firefox") sur la marche à suivre dans Firefox. D’autres instructions pour les autres navigateurs doivent aussi être facilement trouvables. Il existe une variante de la deuxième méthode, qui consiste à se rendre sur le site du [projet Mycroft](http://mycroftproject.com/search-engines.html "Le site du projet MyCroft"), véritable annuaire de plugins de recherche compatibles avec OpenSearch.

# Générer un plugin de recherche pour votre organisation à l’aide de Mycroft

La méthode que je décris ici concerne spécifiquement les collections d’ISIDORE, mais elle est très facilement adaptable pour tout site disposant d’un moteur de recherche.

![Récupérer l'identifiant numérique de la collection sur ISIDORE]({static}/images/identifiant_isidore.png)

1\. En premier lieu, il vous faut récupérer l’identifiant numérique sous lequel votre institution ou collection est référencée sur le moteur de recherche ISIDORE. Rendez-vous sur [la plate-forme](http://www.rechercheisidore.fr/ "La plate-forme ISIDORE"), et dans la colonne de gauche cliquez sur «&#8239;Par collections et organisations&#8239;» puis sélectionnez la vôtre (ou celle qui vous intéresse…). Regardez la barre d’adresse de votre navigateur. L’identifiant est le numéro qui se trouve à la fin de l’URL, juste après **source_taxo=**. Notez-le.

2\. Rendez-vous sur la page du projet Mycroft permettant de [générer un plugin](http://mycroftproject.com/submitos.html "Proposer un plugin sur Mycroft"). Remplissez les champs comme suit&#8239;:

1.  **Plugin name&#8239;:** mettez quelque chose comme **<institution> (via ISIDORE)**. Exemple&#8239;: REGARDS (via ISIDORE).
2.  **Plugin description&#8239;:** Il faut quelque chose de court. Je n’ai jamais vu le texte entré ici apparaître quelque part, mais sait-on jamais…
3.  **Search URL&#8239;:** C’est la partie la plus importante. Entrez **http://www.rechercheisidore.fr/search?q={searchTerms}&source_taxo=<identifiant_institution>** en remplaçant <identifiant_institution> par le numéro que vous avez récupéré à l’étape 1\. La puce **GET** doit être cochée. Exemple&#8239;: http://www.rechercheisidore.fr/search?q={searchTerms}&source_taxo=18144929
4.  **Input encoding**&#8239;: Laissez tel quel, c’est-à-dire avec la valeur **UTF-8**.
5.  **Suggestions URL&#8239;:** Entrez **http://www.rechercheisidore.fr/suggest/?q={searchTerms}** Les suggestions sont globales (elles ne concernent pas qu’une seule collection). Il ne sert donc à rien de rajouter l’identifiant.
6.  **Search form URL&#8239;:** Entrez **http://www.rechercheisidore.fr/search?source_taxo=<identifiant_institution>** Exemple&#8239;: http://www.rechercheisidore.fr/search?source_taxo=18144929
7.  **Reference&#8239;:** Il faut trouver une suite alphanumérique qui décrive votre plugin et qui soit unique sur Mycroft. Je vous suggère quelque chose comme **isidore-<institution>** Exemple&#8239;: isidore-regards
8.  **Select icon&#8239;:** Il s’agit de l’icône qui illustrera votre plugin dans la barre de recherche. Soit vous désirez utiliser le favicon du site de votre institution, soit vous préférez opter pour celui d’ISIDORE. Dans ce cas, il vous suffit de cocher **«&#8239;Copy existing plugin&#8239;»** et d’entrer **isidore-regards** dans le champ de texte.
9.  **Plugin category&#8239;:** Il n’y a rien de vraiment adéquat. Libraries ou Education > Universities peuvent faire l’affaire selon les cas.
10.  Je ne vous aide pas pour le reste des champs. 😉

Générez le plugin, testez-le (l’icône ne sera pas celle que vous avez choisie mais une tête de tyrannosaure rouge) et si tout fonctionne comme prévu proposez votre plugin (votre icône sera alors la bonne). Pour remplacer le plugin de test par le définitif, il faut supprimer manuellement le premier de votre liste de plugins de recherche. Si votre plugin n’apparaît pas lorsque vous le cherchez sur Mycroft,cliquez sur «&#8239;more…&#8239;» sous la ligne «&#8239;Site language&#8239;» et cochez la case «&#8239;Skip Cache&#8239;».

# Intégrer ce plugin sur la page de votre institution (optionnel)

La méthode proposée ci-dessus possède deux inconvénients. D’abord, pour qu’un utilisateur puisse ajouter votre plugin, il faut déjà qu’il connaisse Mycroft. Ensuite, si Mycroft ferme, il deviendra indisponible pour de nouveaux utilisateurs.

Les deux problèmes se résolvent simultanément. Pour ne plus dépendre de Mycroft, il faut héberger le plugin directement sur le serveur de votre institution (il faut donc posséder les droits nécessaires). Le plus simple est d’aller dans le [dossier de votre profil Firefox](http://www.commentcamarche.net/faq/14345-firefox-trouver-le-dossier-du-profil "Trouver le dossier de votre profil Firefox"), puis dans le répertoire **searchplugins** pour y trouver le vôtre. Il vous suffit alors de l’uploader sur votre serveur.

Pour que les visiteurs de votre site puisse installer le plugin que vous venez d’uploader, vous pouvez ajouter le texte suivant dans la section **head** d’une ou de plusieurs pages de votre site&#8239;:

<pre> <link rel="search" type="application/opensearchdescription+xml" title="<Plugin name>" href="<l'URL du fichier XML>" /></pre>

Remplacez **<Plugin name>** par la valeur que vous avez entrée plus haut comme Plugin name, et **<l’URL de votre fichier XML>** par l’adresse adéquate. Enregistrez, c’est terminé.

J’espère que mes explications sont claires, et que vous n’avez pas rencontré de difficulté au cours des différentes étapes. Néanmoins, le cas échéant, n’hésitez pas à poser votre question en commentaire.
