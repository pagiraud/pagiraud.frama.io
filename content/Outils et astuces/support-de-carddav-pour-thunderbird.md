Title: Support de CardDav pour Thunderbird
Date: 2012-03-19 8:00
Tags:CardDav, Thunderbird, SOGO Connector
Slug:support-de-carddav-pour-thunderbird
Je me suis installé un serveur [ownCloud](http://www.owncloud.org "ownCloud"), dont je suis assez content. Parmi les nombreuses fonctionnalités du logiciel, on trouve la gestion des carnets d’adresses grâce au protocole CardDav, qui permet la synchronisation des-dits carnets avec divers logiciels ou matériels (_smartphones_, tablettes, etc.).

Seul problème, Thunderbird, mon client de messagerie, ne supporte pas CardDav en natif. Néanmoins, une solution existe grâce à une extension (non répertoriée sur le site de Mozilla)&#8239;: [SOGO connector](http://www.sogo.nu/fr/downloads/frontends.html "SOGO Connectors").

Elle fonctionne très bien, j’ai donc pu me débarrasser de la solution que j’utilisais avant (Zimbra Mail de Free couplé à Zindus) et continuer ainsi la reprise de contrôle de mes données personnelles.

Une fois l’extension installée, ouvrez votre carnet d’adresses puis rendez-vous sur **Fichier&#8239;> Nouveau&#8239;> Carnet d’adresses distant**. Renseignez alors le nom du carnet à votre convenance puis l’URL.

Toutefois, ça ne vaudra jamais une intégration pleine et entière à Thunderbird. Faites vous entendre sur [Bugzilla](https://bugzilla.mozilla.org/show_bug.cgi?id=546932 "Add support for online address books using the CardDav format.")&#8239;!
