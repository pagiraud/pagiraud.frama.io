Title: Publier ses tweets en tant que billets
Date: 2012-07-22 8:00
Tags: Twitter, Archive, Plugin Wordpress

J’ai cherché longtemps un plugin WordPress capable de récupérer mes tweets et de les transformer en billets normaux. J’en ai enfin trouvé un qui me convient, même si, vous allez le voir, quelques problèmes demeurent. J’espère que ce billet permettra d’écourter la recherche des blogueurs intéressés par ce type de service.

# Tout est une question de vocabulaire

J’ai cherché les expressions suivantes&#8239;:

*   publish tweets as posts
*   tweets as wordpress posts
*   tweets to posts wordpress plugin

Sans aucun résultat… Je les reproduis ici pour qu’en les cherchant on puisse justement tomber au moins sur cette page. Puis j’ai compris qu’il fallait plutôt chercher&#8239;:

*   import tweets to wordpress
*   archive tweets into wordpress
*   wordpress twitter archiver

Ou d’autres choses du même acabit.

# Ozh’ Tweet Archiver, une solution sous-optimale

Quant à moi, j’ai finalement opté pour [Ozh’ Tweet Archiver](http://wordpress.org/extend/plugins/ozh-tweet-archiver/ "Plugin pour archiver ses tweets dans WordPress"). Il accomplit sa mission (presque) à la perfection. J’ai en effet repéré deux limitations, dont je suis parvenu à contourner la première.

## The medium is the message

Les tweets utilisent forcément le [format de billet](http://codex.wordpress.org/Post_Formats "Mise au point sur les formats de posts") ‘standard’ (‘Par défaut’ en français, à différencier du format que vous avez configuré pour être celui… par défaut). Même en demandant à ce que ‘aside’ (‘En passant’) soit le format par défaut, les tweets utilisent tout de même ‘standard’. Cela est probablement dû au fait que le plugin ait été mis à jour pour la dernière fois avant que les formats de billets fassent leur apparition dans WordPress.

Quoi qu’il en soit, la mise en page qui en résulte est loin d’être optimale&#8239;: le titre du billet-tweet (qui est le même que son contenu) prend une place démesurée (c’est fou ce que 140 caractères peuvent être longs&#8239;!). Pour résoudre le problème, il convient d’ajouter deux lignes au fichier `inc/import.php`&#8239;:

	$postformat = 'aside';
	set_post_format( $post_id , $postformat);

Il faut placer ces lignes après&#8239;:

	$post_id = wp_insert_post( $post );

## Couper des billets à la hash

Le deuxième problème est plus ardu (je n’ai pas encore réussi à le résoudre). Si vous choisissez de transformer les hashtags en mots-clés, que ce soit pour rediriger vers une recherche Twitter ou une recherche interne à votre blog, alors Ozh’ Tweet Archiver transformera tous les dièses de votre site en mots-clefs (remarquez que j’évite soigneusement ce caractère ici). Peu importe? Cela rend inutilisable, entre autres, la fonctionnalité «&#8239;More&#8239;».

Je me pencherai sur la question quand j’aurai davantage de temps.

# Ozhez&#8239;!

Il y a peut-être d’autres solutions, plus complètes, mieux finies, mais je ne les ai pas trouvées. Il y a bien [Tweet Blender](http://wordpress.org/extend/plugins/tweet-blender/ "Tweet Blender") qui offre (entre autres) une solution d’archivage, mais le moyen est disproportionné au regard de mes besoins. Si vous trouvez mieux que Ozh’ Tweet Archiver, faites-moi signe&#8239;! En attendant, je m’en contenterai et essaierai de la bidouiller.
