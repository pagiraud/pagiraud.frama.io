Title: Chercher dans des PDF multiples sous GNU/Linux
Date: 2012-04-12 8:00
Tags: PDF, Command Line Interface (CLI), Recherche de fichier

Vous avez une floppée de PDF, peut-être des centaines, et vous cherchez à savoir quels sont ceux qui utilisent un mot ou une expression donnée. Ou bien, vous essayez de retrouver une citation bien précise —&#8239;qui se trouve dans l’un de ces documents&#8239;— mais vous ne savez pas ou chercher. Comment résoudre ce problème sous GNU/Linux&#8239;?

# Quelques solutions insatisfaisantes à mon avis…

… mais qui vous combleront peut-être.

Il existe des outils très généralistes, qui permettent de chercher un mot ou une expression dans de multiples formats de documents, comme [Beagle](https://en.wikipedia.org/wiki/Beagle_%28software%29 "Beagle"), [Recoll](http://www.lesbonscomptes.com/recoll/ "Recoll est un outil personnel de recherche textuelle pour Unix et Linux"), ou tous les logiciels basés sur <del>Zeitgeist</del> [Gnome Activity Journal](https://live.gnome.org/action/show/GnomeActivityJournal "Gnome Activity Journal"). Le problème est qu’ils se fondent sur un système d’indexation beaucoup plus profond que celui du célèbre [locate](https://fr.wikipedia.org/wiki/Locate), et sont même accusés par certains d’être de véritables Big Brothers.

À l’opposé, il est possible de s’en tirer en combinant deux bonnes vieilles commandes. Pendant assez longtemps j’ai utilisé&#8239;:

	less *.pdf | grep mot_cherché

Cette solution est cependant très loin d’être optimale. Le temps d’exécution est plutôt long, la lisibilité de la sortie est mauvaise.

On peut aussi utiliser les outils intégrés à Adobe Reader 9. Non seulement le logiciel n’est pas libre, mais en plus il n’est pas possible de sauvegarder le résultat de la recherche ou d’utiliser des expressions rationnelles.

# pdfgrep, le meilleur compromis entre légèreté, non-intrusion et possibilités

[pdfgrep](http://pdfgrep.sourceforge.net/ "pdfgrep") est pour moi la meilleure façon de réaliser des recherches, y compris complexes (c’est-à-dire à l’aide d’expressions rationnelles) dans de nombreux fichiers PDF sous GNU/Linux. La sortie précise de quel fichier le résultat est issu, et surligne l’expression recherchée. Que demander de plus&#8239;? Ah oui, comme le résultat est affiché sur la sortie standard (stdout), il est très facile d’enregistrer le résultat dans un fichier.

L’usage de base est très simple :

	pdfgrep [option] pattern file

Par exemple :

	pdfgrep -n hacker Coleman*

L’option `-n` permet d’afficher le numéro de la page où se trouve la ligne renvoyée. Ici, pdfgrep renverra toutes les lignes où apparaît la chaîne « hacker » dans tous les fichiers du dossier courant dont le nom commence par «&#8239;Coleman&#8239;».

pdfgrep est dans les dépôts par défaut de toutes les bonnes distributions.
