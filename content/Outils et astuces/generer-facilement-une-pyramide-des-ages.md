Title: Générer facilement une pyramide des âges
Date: 2011-09-07 8:00
Tags: LibreOffice, plugins, enseignement
Slug: generer-facilement-une-pyramide-des-ages 

**Mise à jour du 26 juillet 2012&#8239;:** Si vous êtes intéressé uniquement par l’une de ces quatre années (1950, 2010, 2050, 2100), vous pouvez aussi vous contenter de récupérer les pyramides pré-générées à partir des données du Wolrd Population Prospect sur [le site de l’ONU](http://esa.un.org/unpd/wpp/population-pyramids/population-pyramids_absolute.htm " Population by age groups and sex (absolute numbers)").

**Article d’origine&#8239;:** Si comme moi vous enseignez la géographie, vous avez sûrement déjà voulu montrer la pyramide des âges d’un pays à vos élèves, sans réussir à en trouver des correctes. Peut-être encore avez-vous réussi à vous procurer les statistiques adéquates sans réussir à les mettre en forme dans votre tableur. Ce que je vous propose ici est un logiciel libre, réalisé par mes soins (mais toute participation de quelque sorte que ce soit est la bienvenue), permettant de générer automatiquement une pyramide des âges à partir des données disponibles sur le site des Nations Unies. Plus précisément, ce logiciel est une extension (ou add-on, ou greffon) pour les suites bureautiques OpenOffice.org (OOo) et LibreOffice (LO). Je vais ici présenter l’installation puis l’usage de Pyramide.

# Installer Pyramide dans OOo/LO

1.  [Téléchargez Pyramide]({static}/autres/Pyramide-current.oxt "Version actuelle de Pyramide") sur votre ordinateur (clic-droit Enregistrer la cible du lien sous…).
2.  Ouvrez le fichier téléchargé en double-cliquant dessus.
3.  Suivez les instructions qui s’affichent à l’écran. Il faut faire défiler la licence jusqu’en bas avant de pouvoir la valider.
4.  Fermez le gestionnaire des extensions.

# Utilisation de Pyramide

Pyramide traite des données disponibles sur le site de l’ONU. On ne peut pas (à l’heure actuelle), importer ces données directement depuis Pyramide. Il faut donc commencer par les récupérer, avant de pouvoir travailler dessus.

## Récupérer les données sur le site de l’ONU

1.  Rendez-vous sur le site des [statistiques démographiques de l’ONU](http://esa.un.org/unpd/wpp/unpp/panel_indicators.htm "World Population Prospect").
2.  Optionnel&#8239;: Mettre cette page dans vos marque-pages (ctrl+d) peut s’avérer intéressant pour y accéder plus facilement, surtout qu’on y trouve plein de renseignements très intéressants.
3.  Dans le menu à ascenseurs de gauche (Select Variables), sélectionnez «&#8239;Population by five year age group and sex&#8239;». C’est le premier, donc ce n’est pas compliqué.
4.  Dans le menu à ascenseur de droite, choisissez tout simplement le pays qui vous intéresse.
5.  En bas de la page, faites en sorte que les valeurs indiquées par «&#8239;Select Start Year&#8239;» et «&#8239;Select End Year&#8239;» soient égales.
6.  Enfin, téléchargez les données en cliquant sur «&#8239;Download as .CSV File&#8239;».

## Générer la pyramide

1.  Ouvrez le fichier que vous venez de télécharger avec OOo/LO, par défaut nommé UNPop.csv (selon votre configuration, le .csv peut ne pas s’afficher).
2.  Là, une fenêtre qui peut faire peur apparaît. N’y prenez pas garde, normalement cliquer sur OK suffit.
3.  Cliquez sur la petite pyramide qui a dû apparaître dans votre barre d’outils, ou bien allez sur Outils&#8239;> Add-ons&#8239;> Générer une pyramide. Add-ons est le dernier de la liste.
4.  Voilà, votre pyramide est générée, vous n’avez plus qu’à l’imprimer, la copier-coller dans le traitement de texte, la modifier à votre goût, etc. Vous remarquerez que le nom du pays est en anglais, comme sur le site de l’ONU. Vous devez donc le modifier manuellement. Plus tard, peut-être, je ferai une table de traduction.
