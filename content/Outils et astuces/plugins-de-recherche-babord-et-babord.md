Title: Plugins de recherche Babord et Babord+
Date: 2011-10-11 8:00
Tags: BaBord, extension, Firefox, OpenSearch
Slug: plugins-de-recherche-babord-et-babord

Je ne sais pas si ça peut intéresser grand monde, mais j’ai fait deux _plugins_ [OpenSearch](http://fr.wikipedia.org/wiki/OpenSearch "Description d'OpenSearch")&#8239;: un pour <acronym title="Base Documentaire des Universités de Bordeaux">Babord</acronym> et un autre pour Babord Plus. Il s’agit des catalogues des bibliothèques universitaires de Bordeaux. Ces _plugins_ sont compatibles avec Mozilla Firefox, Microsot Internet Explorer, Google Chrome, et Apple Safari grâce à [une extension](http://www.opensearchforsafari.com/ "OpenSearch pour Safari"). Malheureusement, Opera n’intègre pas OpenSearch.

J’ai publié ces deux _plugins_ sur [Mycroft](http://mycroft.mozdev.org/search-engines.html?name=babord "Mycroft Project"), où vous trouverez une quantité faramineuse d’autres _plugins_ OpenSearch, comme celui du <acronym title="Système Universitaire de documentation">SUDOC</acronym>.

[Mes deux _plugins_ sont fantastiques.](http://mycroft.mozdev.org/search-engines.html?name=babord "Plugins Babord et Babord Plus")
