Title: Plugins de recherche pour la plate-forme Isidore
Date: 2013-06-03 8:00
Tags: ISIDORE, OpenSearch, plugins
Slug: plugins-recherche-isidore
J’aime particulièrement les plugins OpenSearch, comme vous pouvez le voir dans [ce billet]({filename}rediger-un-plugin-de-recherche-integre-au-navigateur.md "Rédiger un plugin de recherche intégré au navigateur") ou encore [celui-là]({filename}plugins-de-recherche-babord-et-babord.md "Plugins de recherche Babord et Babord+"). Je trouve aussi les fonctionnalités de la plate-forme [Isidore](http://www.rechercheisidore.fr/ "La plate-forme ISIDORE") très intéressantes. La seule fonctionnalité qui me semble manquer est la possibilité de pouvoir ne renvoyer par défaut que les ressources accessibles physiquement. Pour mes propres besoins, j’avais rédigé un plugin ne renvoyant que les résultats référencés par REGARDS, le centre de recherche de mon laboratoire. Puis j’ai expliqué ma démarche dans [un billet]({filename}rediger-un-plugin-de-recherche-integre-au-navigateur.md "Rédiger un plugin de recherche intégré au navigateur").

Plusieurs personnes m’ayant dit que le tutoriel était assez compliqué, et vu que le processus me semblait extrêmement simple à automatiser, j’ai créé une page sur laquelle se trouve une liste de plugins de recherche pour la plate-forme Isidore. Il y a un plugin par collection moissonnée, et la liste est automatiquement mise à jour chaque lundi.

Il vous suffit de cliquer sur le bouton correspondant à la collection qui vous intéresse pour installer le plugin !

[La page se trouve ici.](http://www.insolit.org/isidore/ "Liste des plugins OpenSearch pour Isidore")
