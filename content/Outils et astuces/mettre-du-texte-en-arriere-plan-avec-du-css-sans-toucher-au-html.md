Title: Mettre du texte en arrière-plan avec du CSS (sans toucher au HTML)
Date: 2012-08-04 8:00
Tags: Développement web, CSS, SVG, le fond et la forme
Slug:mettre-du-texte-en-arriere-plan-avec-du-css-sans-toucher-au-html
Il m’est arrivé à plusieurs reprises de vouloir mettre du texte en arrière-plan sur une page web. Certains disent que le mieux est de mettre son texte sur une [image matricielle](https://fr.wikipedia.org/wiki/Image_matricielle "Image matricielle") (format PNG par exemple), et d’indiquer la fichier comme `background-image` dans le CSS. C’est une solution assez simple mais qui s’avère peu flexible (Et si je n’ai qu’un éditeur de texte sous la main? Et si je veux définir les mots dynamiquement, en PHP par exemple, sans passer par une bibliothèque du type [ImageMagick](http://www.imagemagick.org "ImageMagick")&#8239;?).

Il existe aussi une [bidouille assez connue](http://stackoverflow.com/questions/1191464/is-there-a-way-to-use-use-text-as-the-background-with-css "Du texte en arrière-plan grâce à un div lui-même placé à l'arrière-plan") impliquant une balise `<div>` dans laquelle on inscrit le texte désiré avant de lui appliquer un `z-index` inférieur à celui courant. Elle a le mérite de répondre aux problèmes posées par l’utilisation d’une image matricielle, mais en pose d’autres. Il est gênant de se servir du HTML, censé traiter la structure du document, pour en assurer la présentation, en principe dévolue au CSS. Il se peut également que je n’ai pas accès au code HTML.

Sachez qu’il existe une troisième solution, qui a le mérite de cumuler les avantages des deux précédentes, sans présenter aucun de leurs inconvénients&#8239;: utiliser la propriété `background` du CSS, mais pour y indiquer l’URL d’un fichier SVG, donc d’une [image vectorielle](https://fr.wikipedia.org/wiki/Image_vectorielle "Image vectorielle"), dans lequel vous aurez au préalable écrit votre texte&#8239;!

# Un exemple simple de code pour se faire une idée

## Les codes sources HTML, CSS et SVG

### Un code HTML tout ce qu’il y a de plus bête

	:::xhtml
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Essai CSS</title>
		<link rel="stylesheet" type="text/css" href="test.css" />
	</head>
	<body id="corps">
	<div><p>Ainsi va la vie</p></div>
	</body>
	</html>

### Le CSS qui s’y rapporte

	:::css
	div {
	background: url('dessin-mini.svg') red;
	border: 1px black solid;
	text-align: center;
	}
	p {
	margin:20px auto 20px auto;
	background: silver;
	padding: 30px;
	text-align: center;
	width: 50%;
	}

### Et enfin le SVG qui va bien

	:::svg
	<?xml version="1.0" encoding="UTF-8"?>
	<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="180" height="32">
	<text y="15">Attention Danger!</text>
	</svg>

On obtient donc un fichier très léger, de quelques centaines d’octets (ici 163), manipulable comme un vulgaire fichier texte&#8239;! Il faut simplement penser à adapter la valeur de `width` en fonction de son texte (faisable simplement en comptant les caractères si on utilise une police à chasse fixe — `font-family: monospace;`).

## Le rendu de l’exemple&#8239;:

<div style="background: url('/images/dessin-mini.svg') red; border: 1px black solid; text-align: center;">
<p style="margin: 20px auto 20px auto; background: silver; padding: 30px; text-align: center; width: 50%;">Ainsi va la vie</p>
</div>

Il est vrai que si le SVG n’est pas souvent évoqué comme possibilité pour les images d’arrière-plan (et encore moins pour les textes) c’est que tous les navigateurs ne sont pas [compatibles nativement](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics#Native_support "Support du SVG par les différents navigateurs"). Les choses vont quand même bien mieux que début 2011 (avant la sortie de Windows Internet Explorer 9, le premier navigateur de chez Microsoft à supporter le SVG).

Notre texte d’arrière-plan est pour l’instant très simple, mais nous disposons de toute la puissance du SVG pour le personnaliser, l’intégrer à des dessins, etc.

# Pour aller plus loin…

## Changer la police, la couleur ou la taille du texte

Le SVG minimal proposé plus haut ne contenait aucune information sur l’apparence à donner au texte. C’est donc celle par défaut qui est appliquée. Mais, si vous voulez le personnaliser, ce n’est pas plus compliqué que d’éditer du CSS&#8239;!

	:::svg
	<?xml version="1.0" encoding="UTF-8"?>
	<svg
		xmlns="http://www.w3.org/2000/svg"
		version="1.1" width="500" height="32">
		<text
			x="0"
			y="31"
			style="font-size:40px;
				font-style:normal;
				font-weight:normal;
				line-height:125%;
				letter-spacing:0px;
				word-spacing:0px;
				fill:#000000;
				fill-opacity:1;
				stroke:none;
				font-family:Sans">
		Mon petit texte à moi</text>
	</svg>

Vous pouvez ainsi modifier à votre guise bon nombre de propriétés de votre texte SVG très facilement.

## Et pour la rotation&#8239;?

Il est courant de vouloir que le texte d’arrière-plan soit incliné de 30° ou 45°. Il existe deux méthodes pour obtenir ce résultat.

### Indiquer la rotation dans le fichier SVG lui-même

C’est la solution qui paraît la plus évidente, mais nous allons voir que ce n’est pas le cas. Elle consiste à appliquer une transformation sur la balise `<text>`&#8239;: `transform = "rotate(-45 90 16)` où -45 correspond à l’inclinaison à donner au texte en degrés (le signe négatif indique qu’il faut tourner dans le sens inverse des aiguilles d’une montre, ce qui n’est [pas logique](https://fr.wikipedia.org/wiki/Sens_direct "Les sens de rotation"), mais je ne fais pas les normes). Les deux nombres qui suivent indiquent les coordonnées du centre de rotation. Ici, je l’ai placé au milieu du SVG. Le code du fichier est donc le suivant&#8239;:

	:::svg
	<?xml version="1.0" encoding="UTF-8"?>
	<svg xmlns="http://www.w3.org/2000/svg" version="1.1"  width="180" height="32">
	<text y="15" transform = "rotate(-45 90 16)">Attention Danger!</text>
	</svg>

Le problème essentiel de cette méthode, c’est qu’elle ne fait pas tourner l’image elle-même, seulement son texte. Il faut donc redimensionner l’image pour voir intégralement le texte, mais bien entendu cela modifie le centre de rotation que vous devriez choisir. Cela devient très vite un casse-tête. Donc, sauf si vous utilisez un logiciel de dessin vectoriel comme [Inkscape](http://inkscape.org/ "Inkscape, logiciel de dessin vectoriel"), je vous déconseille d’utiliser cette méthode.

### Demander au CSS de faire pivoter l’arrière-plan

En réalité, la solution la plus simple est encore de passer par le CSS pour appliquer la rotation. Dès lors, il s’agit de faire tourner notre texte SVG comme on le ferait de n’importe quelle image d’arrière-plan. Là aussi, des limitations existent, mais elles sont bien moindres. Cela peut devenir un problème seulement si on souhaite mettre le texte en arrière-plan de `<body>`. Seulement, il s’agit là de tout un programme, et le plus simple est encore de vous renvoyer vers un tutoriel sur le sujet déjà bien rédigé, comme [celui-ci](http://www.sitepoint.com/css3-transform-background-image/ "How to Apply CSS3 Transformations to Background Images") (en anglais).

## Des personnalisations très poussées

Vous avez toute la puissance du CSS désormais à disposition. Le plus simple pour avoir des fichiers assez légers et très personnalisés est de les réaliser sous Inkscape et de les enregistrer en SVG simple (ce qui n’est pas le format par défaut). Cela permet par exemple de positionner le texte sur des chemins, ou d’y appliquer des transformations complexes. Vous pouvez également essayer d’implémenter à la main quelques transformations grâce à [quelques exemples](http://www.w3schools.com/svg/svg_text.asp "Texte et SVG") fournis par W3Schools.

# Pour finir&#8239;: le W3C, McLuhan et Apollinaire

<figure>
<img src="/images/apollinaire_colombe.jpg" alt="Apollinaire&#8239;: La colombe poignardée et le jet d'eau" title = "Apollinaire&#8239;: La colombe poignardée et le jet d'eau"/>
<figcaption>Apollinaire&#8239;: La colombe poignardée et le jet d’eau</figcaption>
</figure>

Mettre du texte en arrière-plan grâce à un fichier SVG présente donc plusieurs avantages. Pourtant, si le W3C n’a jamais voulu permettre l’écriture dans le CSS (à l’exception notable des pseudo-éléments `:before` et `:after`), c’est que pour lui la séparation du fond et de la forme doit être la plus nette possible. Or cette séparation n’est qu’une fiction (qui suppose la séparabilité du message et de son support, voire l’absence d’influence du deuxième sur le premier) qui trouve avant tout sa justification dans la division des tâches au sein des entreprises de développement web. Elle est également un pré-supposé des vastes campagnes de numérisation, et se retrouve même dans les propos de philosophes tels Pierre Lévy lorsqu’il affirme que le virtuel est le réel en puissance (ce qui signifie pour lui que tout doit être et sera numérisé).

Pourtant, il est impossible de séparer le message de sa présentation et de son support, les deux derniers contribuant tout autant à la construction du sens que la partie verbale du message (sans compter qu’il serait possible de renouveler cette dichotomie fond/forme pour le texte lui-même). Il y a bien sûr le célèbre aphorisme de McLuhan, «&#8239;[The medium is the message](https://en.wikipedia.org/wiki/The_medium_is_the_message "The medium is the message, de Marshall McLuhan")&#8239;», mais il faut aussi évoquer l’œuvre de [Guillaume Apollinaire](https://fr.wikipedia.org/wiki/Guillaume_Apollinaire "Guillaume Apollinaire"), dont le travail sur les calligrammes rend évident la participation de la présentation à la construction du sens. On retrouve des expérimentations de la même veine dans le _net art_ et même dans l’[ASCII art](https://fr.wikipedia.org/wiki/Ascii_art "L'ASCII Art, de l'Amiga aux smileys")&#8239;!
