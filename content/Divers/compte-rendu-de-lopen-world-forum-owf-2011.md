Title: Compte-rendu de l’Open World Forum 2011
Date: 2011-10-09 8:00
Tags: cloud, Microsoft, mutualisation, open source, Open World Forum, opendata, Oracle, owf, RedHat
Slug: compte-rendu-de-lopen-world-forum-owf-2011 

L’<acronym title="Open World Forum">OWF</acronym> a lieu chaque année depuis 2008. Le but de [ce forum](http://www.openworldforum.org/fre/footer/A-propos-de-l-Open-World-Forum "À propos de l'Open World Forum") est de dynamiser le monde du Libre par des rencontres entre tous les acteurs majeurs du secteur&#8239;: industries, administrations, associations et développeurs. Son comité de pilotage, comprenant entre autres la _Linux Foundation_, l’<acronym title="Association Française des utilisateurs de Linux">AFUL</acronym> et la mairie de Paris, est à l’image de cet objectif. Avec près de 2000 participants, il se veut l’événement du genre le plus important d’Europe.  
Comme son nom le laisse supposer, l’OWF ne se limite pas à l’_open source_, mais traite aussi de toutes ses modulations, pour reprendre le terme employé par Christopher Kelty&#8239;: _Open Data_, _Open Standards_, _Open Hardware_, _Open Innovation_, _Open Governement_, etc. Le slogan de l’édition 2011 est à ce titre éclairant&#8239;: _Open Everything_. Le sens très inclusif donné à l’adjectif _open_ justifie la participation voire le [parrainage](http://www.openworldforum.org/index.php/fre/Sponsors "Sponsors de l'OWF 2011") d’entreprises dont le rapport au logiciel libre est au moins ambigu, comme Microsoft ou Oracle.

L’édition 2011 s’est déroulée du 22 au 24 septembre à Eurosite Georges V, dans le VIII<sup>e</sup> arrondissement de Paris. Environ 200 communications ont été données, réparties entre la salle principale (_keynotes_ et tables rondes) et une dizaine de petites salles (présentations spécialisées aux échanges plus nourris). L’OWF 2011 était organisé autour de trois fils rouges (_threads_)&#8239;:

*   _think_, à propos de l’avenir du Libre et de ses implications économiques, politiques ou sociétales&#8239;;
*   _code_, où des développeurs présentaient leurs travaux et proposaient des astuces de programmation&#8239;;
*   _experiment_, destiné au grand public, qui permettait de tester des logiciels et des robots, de participer à des ateliers de fabrication assistée par ordinateur ou encore de visiter des expositions d’œuvres libres.

À la lecture détaillée du [programme](http://www.openworldforum.org/index.php/fre/Univers/Schedule "Programme de l'OWF"), plusieurs sujets centraux émergent. Nous les avons classé ci-dessous dans ce qui nous semble être leur ordre croissant d’importance.

# Une plate-forme publicitaire

Les principaux _sponsors_ se sont servi de l’OWF comme plate-forme publicitaire. Certains ont animé des _keynotes_ de présentation de leurs produits (Red Hat, Suse, Canonical, Microsoft, etc.). Leur présence, d’un point de vue très général, s’est beaucoup faite sentir tout au long du forum, notamment celle de Red Hat.

# Un pari sur le _cloud_

Le _cloud_ a occupé 3 _keynotes_, 2 tables rondes et 5 sommets (_summits_), impliquant au total près d’une trentaine de présentateurs. Les communications à ce sujet étaient assez techniques, tournant essentiellement autour des questions de résilience du _cloud_ (Comment faire pour que les pannes ne soient pas ressenties par l’utilisateur&#8239;?) et de l’interopérabilité (Comment se démarquer sur le marché tout en laissant la possibilité au client de changer de prestataire lorsqu’il le souhaite&#8239;?).

# Le mobile comme terminal d’avenir

Les terminaux mobiles (_smartphones_ et tablettes) ont été abordés dans une table ronde et 4 _summits_. Des intervenants concernés par tous les aspects du sujet sont intervenus, depuis les fabricants de matériel comme Archos jusqu’aux communautés de développeurs (<acronym title="Paris Android User Group">PAUG</acronym>, Meego France, qui a des chances de devenir Tizen France suite au [nouveau partenariat entre Intel et Samsung](http://arstechnica.com/open-source/news/2011/09/meego-rebooted-as-intel-and-samsung-launch-new-tizen-platform.ars "Meego devient Tizen"), en passant par des opérateurs (Orange). Les avantages de l’_open source_ dans ce secteur (grande liberté des développeurs) ont été présentés, ainsi que certaines difficultés rencontrées (hétérogénéité des terminaux sous Android, augmentant la durée et le coût des phases de test). Les communications sur les terminaux mobiles étaient très liées à celles sur la neutralité d’internet, qui a occupé plusieurs tables rondes.

# L’_Open Data_ au cœur des nouveaux enjeux sociétaux

L’_Open Data_ était probablement le thème explicite majeur de cette édition 2011&#8239;: 8 _keynotes_ et 3 _summits_ l’abordaient directement, soit au total plus de 30 communications, sans compter les présentations qui abordaient le sujet de manière plus secondaire.

## Les vertus de l’_Open Data_ font consensus

Les présentateurs, souvent porteurs de projets, ont insisté sur la dimension politique et sociétale de l’_Open Data_, devant aboutir à un _Open Government_ et reposer sur une _Open Innovation_, le tout formant des _Open Minds_. La liste des bénéfices de l’_Open Data_ établie par Nigel Shadbolt, qui conduit l’ouverture des données publiques au Royaume-Uni, résume assez bien les arguments des différents participants&#8239;:

1.  une plus grande transparence de l’administration&#8239;;
2.  une hausse de l’engagement des citoyens dans la vie de la communauté&#8239;;
3.  une amélioration des services publics&#8239;;
4.  des données de meilleure qualité (correction par les citoyens)&#8239;;
5.  meilleure efficacité.

Claire Gallon, co-fondatrice de [LiberTIC](http://libertic.wordpress.com/libertic/ "LiberTIC"), a dressé un tableau similaire. Cette association a lancé l’ouverture des données de la municipalité nantaise. Pour Claire Gallon, l’_Open Data_ permet en outre aux collectivités de redécouvrir leurs propres fonds de données, qui sont souvent des «&#8239;mines d’or inexploitées&#8239;». Cependant, elle a également souligné la lenteur de la mise en place du projet, due pour partie à des résistances.

## Des collectivités plutôt hésitantes face à la démarche.

Hervé Rannou, co-fondateur de [Share-PSI](http://share-psi.eu/ "Share PSI") et de l’OWF, a développé cet aspect. Il agit pour l’ouverture des données dans la Communauté Urbaine Marseille Provence Métropole, dans le cadre de Marseille, capitale européenne de la culture 2013\. L’_Open Data_ a été choisie par les institutions pour permettre une meilleure coordination des opérations et des <acronym title="Direction des Services Informatiques">DSI</acronym>. Il a compté 7 causes principales de résistance, parmi lesquelles la peur de perdre du pouvoir, la question de la responsabilité en cas d’erreur ou de dysfonctionnement, ou encore le coût même de la mise en place d’une plate-forme. Il a évoqué quelques pistes pour répondre à ces interrogations.

Une des résistances majeures selon lui demeure le refus ou la crainte que les données soient utilisées pour faire du profit. Cependant, il parvient à convaincre élus et administrations en leur rappelant que le développement local fait partie de leurs attributions, et que cela permet de mieux valoriser de l’argent public déjà dépensé.

## Un nouveau créneau porteur pour les entreprises

Les différents participants ont également mis l’accent sur les retombées économiques de l’_Open Data_. L’idée centrale pourrait être résumée ainsi&#8239;: «&#8239;Libérez vos données, et les applications suivront&#8239;» (Nigel Shadbolt). Pour Claire Gallon, néanmoins, la difficulté réside dans l’évaluation du succès de l’ouverture, qui ne peut être mesurée que par l’intensité des réutilisations. Pour mettre en évidence les retombées économiques de l’_Open Data_, LiberTIC organise régulièrement des événements et des concours mettant en valeur les entreprises qui réutilisent les données nantaises.

Plusieurs start-up ont profité des _summits_ pour présenter leurs projets. [Data Publica](http://www.data-publica.com/ "Data Publica"), dirigée par Christian Frisch, est l’une d’elles. Le service, lancé en septembre 2010, propose des jeux de données fondés sur des contenus spécifiques. Chrsitian Frisch affirme que Data Publica est le troisième site du genre le plus important au monde.

## Une ouverture très inachevée

Cependant, et bien que respectant les licences des données qu’il agrège, Data Publica propose ses jeux dans un format fermé et non normalisé, à savoir le XLS de Microsoft. Or, les intervenants ont tous souligné la nécessité d’utiliser des standards ouverts pour que l’_Open Data_ ait un sens. Hervé Rannou, par exemple, a pointé du doigt leur faible utilisation par les administrations, rendant certaines données inexploitables voire illisibles&#8239;:

> Parfois, l’entreprise à l’origine du format a disparu, et trouver qui détient les droits peut devenir un vrai problème.

L’autre écueil de l’_Open Data_ est représenté par la rareté des données structurées. L’exemple le plus souvent cité est celui de l’abondance des <acronym title="Portable Document Format">PDF</acronym>, y compris lorsqu’il contient des pages entières de tableaux. En effet, il est très difficile d’interroger efficacement un PDF pour en extraire les données souhaitées. Pour Jan Wildeboer (Red Hat), les données publiques libérées ne sont ainsi bien souvent que «&#8239;des cadavres réunis dans de vastes cimetières&#8239;».

Enfin, lors d’une table ronde, Fabien Gandon (<acronym title="Institut national de recherche en informatique et en automatique">INRIA</acronym>), a soulevé le problème de la _Big Data_, c’est-à-dire d’une abondance de données telle que les machines ne peuvent les traiter efficacement. Si quelques pistes existent autour des bases NoSQL, il faut selon lui «&#8239;réinventer la grammaire et la manière de lire les données&#8239;» afin que l’_Open Data_ ne soit pas «&#8239;qu’un argument marketing&#8239;».

# L’utilisateur au cœur du nouveau modèle de production de l’open source

Ce dernier axe, pourtant très sensible dans la plupart des communications, n’a pas été formalisé par un _summit_ ou une table ronde. Il s’agit plutôt d’une tendance transversale implicite, peut-être car elle n’est que rarement le fruit des acteurs traditionnels du Libre.

Pour Jean-Paul Planchou, vice-président de la région Île de France, l’_open source_ «&#8239;conforte l’idée que seuls le partage et les croisements permettent l’innovation&#8239;» et ainsi les pratiques issues du Libre «&#8239;innervent de plus en plus les politiques publiques&#8239;». L’_open source_ a donc remporté une «&#8239;victoire culturelle&#8239;». Cependant, les administrations n’ont pas été conçues pour fonctionner sur un mode participatif. Dans le cadre de l’_Open Data_, par exemple, elles ne sont pas aptes à prendre en compte toutes les demandes de corrections, d’ajout ou de suppression de données venues d’acteurs externes. À Nantes, LiberTIC sert à la fois de filtre et de lien.

Si l’_open source_ modifie les pratiques des administrations en apportant davantage d’horizontalité et de participation dans la prise de décision, les modèles de développement logiciels se sont également transformés. De nombreux projets témoignent de ce qu’on pourrait appeler une dé-technicisation de la _roadmap_ (feuille de route)&#8239;: les utilisateurs professionnels deviennent les maîtres d’ouvrage et les développeurs les maîtres d’œuvre. Il y a donc une véritable intégration de l’_open source_ à la culture dominante.

## Des consortiums intégrant les clients

Parmi les projets industriels, [<acronym title="Open Platform for the Engineering of Embedded Systems">OPEES</acronym>](http://www.opees.org/ "Open Platform for the Engineering of Embedded Systems") est le plus symptomatique de cette dynamique. Cette plate-forme met en relation des constructeurs ayant besoin de maintenait et développer des logiciels sur des systèmes embarqués sur une très longue période (plusieurs dizaines d’années), avec des <acronym title="Société de Services en Logiciels Libres">SSLL</acronym> comme [Obeo](http://obeo.fr/pages/logiciel-libre/fr "Obeo"). Or, les logiciels propriétaires sont au mieux maintenus pendant 10 ans. OPEES a été initié par Airbus, ce qui révèle le dynamisme du Libre dans l’_Aerospace Valley_, et donc en Aquitaine. Cependant, OPEES vise à intégrer des industriels d’autres secteurs, comme le nucléaire. Gaël Blondelle voit dans la formation d’OPEES le signe de l’avènement d’une nouvelle génération de modèle économique du Libre.

## La généralisation des mutualisations

Sans aller jusqu’à la formation d’un consortium, la mutualisation relève de la même tendance. Celle-ci émerge depuis plusieurs années maintenant grâce à des associations comme l’<acronym title="Association des développeurs et utilisateurs de logiciels libres pour les administrations et les collectivités territoriales">ADULLACT</acronym>, mais déborde dorénavant dans bien d’autres secteurs. Lors du _summit_ sur l’_Humanitarian <acronym title="Free/Open Source Software">FOSS</acronym>_, plusieurs communications en ont témoigné. Par exemple, 11 <acronym title="Organisation Non Gouvernementale">ONG</acronym> se sont associées dans le projet [Sigmah](http://www.sigmah.org/ "Sigmah"), logiciel de management de l’information. On peut compter, entre autres, parmi ces ONG&#8239;: Action contre la faim, la Croix rouge française, Handicap International ou encore Médecins du Monde.

L’initiateur est le groupe <acronym title="Urgence, Réhabilitation, Développement">URD</acronym>, qui a commencé par persuader les ONG de la convergence de leurs besoins, et que la mutualisation des coûts de développement était donc possible. Olivier Sarrat a insisté sur l’importance de la participation des utilisateurs aux procédures de développement, que ce soit au niveau de la gestion de la feuille de route ou du retour d’expérience. Selon lui, «&#8239;les efforts de participation expliquent la très forte adoption du projet par les ONG&#8239;». Si les développeurs se contentent de la mise en œuvre, ce qui constitue pour eux un certain choc culturel, les utilisateurs ont dû quant à eux formaliser leurs procédures, leur façon de travailler, afin que les programmeurs puissent les implémenter dans Sigmah. Ils ont pour cela adopté une méthode itérative, caractéristique du modèle de développement _open source_&#8239;: l’acculturation a bien lieu dans les deux sens.

Ces exemples montrent que, progressivement, les firmes et les organisations de toutes sortes se saisissent des possibilités du Libre, cessent d’être «&#8239;des consommateurs passifs&#8239;» (Anne Nicolas, [Hupstream](http://www.hupstream.com/fr/ "Hupstream")). En intégrant les pratiques de l’_open source_, elles accèdent à des bénéfices qui vont bien au-delà d’une simple économie budgétaire sur les licences.

# Conclusion

L’édition 2011 de l’OWF a démontré une fois de plus le dynamisme du Libre en France et dans le monde. En des temps incertains, il est l’un des rares secteurs en pleine croissance. Ainsi, Red Hat, principal parrain de l’événement, a un résultat d’exploitation de $76,4 millions au deuxième trimestre 2011, en hausse de 41% par rapport à l’an dernier. Red Hat prévoit de dépasser allègrement le milliard de chiffre d’affaire pour 2012. Il ne faudrait cependant pas résumer le Libre à sa dimension industrielle&#8239;: il s’agit d’un véritable paradigme sociétal, visible par les constructions territoriales de ses acteurs.