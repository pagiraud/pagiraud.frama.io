Title: Radio Giroll&nbsp;: Microsoft à L’Open World Forum
Date: 2012-04-8 8:00
Tags: Microsoft, open world forum, owf, radio giroll
Slug: radio-giroll-microsoft-open-world-forum
Jeudi dernier (5 avril 2012), c’était la première [Radio Giroll](http://www.giroll.org/post/2012/03/30/Radio1-jeudi-5-avril "Première Radio Giroll") diffusée sur [Oxyradio](http://www.oxyradio.net/ "Oxyradio - Webradio libre - Respire et Écoute"). Enfin première, pas vraiment : cela fait bien longtemps que le collectif diffuse des émissions de radio sur Oxy à l’occasion de ses <del>légendaires</del> sympathiques _install-parties_. C’est à l’occasion de celle de décembre (Oneiric Party) que [j’ai interviewé]({filename}/Giroll/interview-jean-christophe-elineau-oneiric-party.md "Interview de Jean-Christophe Élineau à l’Oneiric Party") Jean-Christophe Élineau d’[Aquinetic](http://www.pole-aquinetic.fr/ "Aquinetic").

Au programme&nbsp;:

*   Un [retour sur le BarCamp](http://www.giroll.org/post/2012/04/05/Radio1-Retour-sur-le-Barcamp2 "Réactions au 2e BarCamp Bordelais") bordelais des 31 mars et 1er avril 2012 par Thomas&nbsp;;
*   [Ubuntu TV et Ubuntu pour Androïd](http://www.giroll.org/post/2012/03/30/Radio1-Ubuntu-TV-Ubuntu-for-Android "Le travail de Canonical sur les form factors") par Mathieu&nbsp;;
*   Un [retour sur la conférence de Benjamin Bayart](http://www.giroll.org/post/2012/03/29/conferences-ateliers-benjamin-bayart-aquilenet "Réactions à la conférence de Benjamin Bayart") du 24 mars au Centre d’Animation Saint-Michel (Bordeaux) par Yorick&nbsp;;
*   [Microsoft à L’Open World Forum](http://www.giroll.org/post/2012/03/31/Radio1-La-strat%C3%A9gie-globale-de-Microsoft-face-aux-logiciels-libres "Microsoft et les logiciels libres") par bibi.

Je ne gère pas encore bien le micro, ce qui explique les variations de volume, et il faut que je baisse le débit&nbsp;: c’est bien plus difficile que de parler à un public présent&nbsp;!

 <audio controls>
  <source src="../audio/radio1_microsoft.ogg" type="audio/ogg">
Your browser does not support the audio element.
</audio> 

Je vous encourage très fortement à écouter les autres émissions sur le site de Giroll, elles sont très intéressantes. Notamment, il est très instructif d’analyser les retours sur la conférence de Benjamin Bayart après avoir écouté ladite conférence, qui sera bientôt disponible chez [Aquilenet](http://www.aquilenet.fr/ "Aquilenet | Fournisseur d'accès Internet libre en Aquitaine").

Copie de l’article publié sur le [site de Giroll](http://www.giroll.org "Site du collectif Giroll")&nbsp;:

Le ton du discours de Microsoft sur les logiciels libres a bien changé. Si en 2001 Steve Ballmer, actuel CEO (PDG) de Microsoft, a affirmé que Linux était un cancer, en 2007 il a annoncé qu’il aimerait que toute innovation dans l'_open source_ ait lieu sous Windows. Beaucoup y voient un réel infléchissement dans la position du géant de l’informatique sur le sujet. Pourtant, les deux positions ne sont pas contradictoires.

* * *

Le géant mondial de l’informatique a une stratégie globale, cohérente, coordonnée pour atteindre son objectif&nbsp;: maintenir sa situation monopolistique dans un marché en mutation perpétuelle.

Plutôt que de brosser à grands traits un portait abstrait de cette stratégie, nous allons développer dans plusieurs épisodes des actions concrètes de Microsoft, leurs impacts sur le monde du Libre, et leur place dans la stratégie globale de la firme.

# Épisode 1&nbsp;: <del>La menace fantôme</del> Microsoft à L’Open World Forum

L'_Open World Forum_ (OWF) est devenu une grand-messe de l'_open source_ en France, voire en Europe. De nombreux responsables industriels et politiques s’intéressant aux logiciels libres, mais aussi à l'_open data_, à l'_open hardware_ et aux cultures libres s’y retrouvent chaque année depuis 2008, à Paris.

Lors de la dernière édition, on pouvait y croiser entre autres Éric Besson (Ministre de l’Industrie, de l’Énergie et de l’Économie numérique), Mark Shuttleworth (fondateur de l’entreprise Canonical qui soutient notamment la distribution GNU/Linux Ubuntu), Nigel Shadbolt (qui travaille avec Tim Berners-Lee sur le projet _open data_ du gouvernement britannique) et Alfonso Castro, directeur de la stratégie interopérabilité chez… Microsoft France.

<iframe width="560" height="315" src="https://www.youtube.com/embed/pZHIao169Bo" frameborder="0" allowfullscreen></iframe>

L’OWF a donc ouvert une tribune à Microsoft pour qu’il puisse évoquer une «&nbsp;_collaboration avec l’open source tous azimuts_&nbsp;». La participation de Microsoft à l’OWF va cependant beaucoup plus loin. En effet, il a obtenu le statut de _Gold Sponsor_, au même titre par exemple que l’INRIA ou SuSE. Cela signifie que Microsoft a contribué au financement de l’OWF, mais cela ne signifie pas que la firme de Redmond soit devenue l’amie des logiciels libres. Loin de là.

Par un langage sybillin et l’emploi d’exemples tronqués qui flirtent avec le mensonge, Microsoft essaie de cacher la réalité de son action et de ses objectifs aux citoyens, aux pouvoirs publics, et même aux développeurs. Par le financement d’événements autour du Libre, il affaiblit les positions de ses détracteurs qui acceptent ses subsides.
