Title: Interview de Jean-Christophe Élineau à l’Oneiric Party
Date: 2012-03-04 8:00
Tags: aquinetic, Aquitaine, économie, logiciel libre, open source
Slug: interview-de-jean-christophe-elineau-a-loneiric-party
Le 12 décembre dernier à eu lieu l’Oneiric Party organisée par le [collectif Giroll](http://www.giroll.org/ "collectif Giroll") au Centre d’Animations Saint-Pierre. À cette occasion, j’ai interviewé Jean-Christophe Élineau, qui finalisait alors le lancement du pôle de compétences [Aquinetic](http://www.pole-aquinetic.fr/ "Aquinetic"). Son pari : faire de l’Aquitaine une région leader dans l’économie du logiciel libre. La mise en place, commencée dès 2009, connut des aléas qui occasionnèrent des ajustements conséquents, par exemple quant à la localisation du siège. Pour bien s’en rendre compte, il suffit de comparer l’état du projet tel que décrit dans l’interview avec ce que j’en dis dans [mon M2]({static}/pdf/memoire-M2.pdf) rédigé il y a 2 ans. Avec une subvention d’amorçage de 133000 € accordée par le Conseil Régional d’Aquitaine et le soutien de très nombreux acteurs, on peut dire que la première phase de son pari est un succès.

 <audio controls>
  <source src="../audio/interview_jcelineau.ogg" type="audio/ogg">
Your browser does not support the audio element.
</audio> 
