Title: Radio Giroll : Pioneer One, une web série libre de qualité
Date: 2012-07-22 8:00
Tags: Creative Commons, crowdfunding, culture libre, radio giroll, webserie
Slug: pioneer-one-une-web-serie-libre-de-qualite
Avec beaucoup de retard, voici la présentation de la série Pioneer One que j’ai faite à l’arrache lors de la [Radio Giroll n°3](http://www.giroll.org/post/2012/06/07/Radio3-%3A-7-juin-2012 "Radio Giroll 3") du 7 juin 2012\. Vous pouvez aussi écouter les autres émissions, toutes très intéressantes :

*   [LibrePlan, une solution de gestion de projet libre](http://www.giroll.org/post/2012/06/07/Radio3-LibrePlan-une-application-pour-g%C3%A9rer-vos-projets-librement "LibrePlan"), par Thomas;
*   [L’impression 3D et les droits d’auteur](http://www.giroll.org/post/2012/06/07/L-impression-3D-et-les-droits-d-auteurs "L'impression 3D et les droits d'auteur");
*   [Comment choisir sa licence libre pour sa musique auto-produite ?](http://www.giroll.org/post/2012/06/04/Radio3-Comment-choisir-sa-licence-libre-pour-sa-musique-auto-produite) par Yza.

Le podcast de ma présentation :

 <audio controls>
  <source src="../audio/radio3_pioneerone.ogg" type="audio/ogg">
Your browser does not support the audio element.
</audio> 

Hormis [Sita Sings the Blues](http://www.sitasingstheblues.com/ "Sita Sings The Blues"), de [Nina Paley](http://blog.ninapaley.com/ "Blog de Nina Paley"), pouvez-vous citer une autre production audio-visuelle de fiction sous licence Creative Commons qui dure plus de cinq minutes? Maintenant, oui : [Pioneer One](http://www.pioneerone.tv/ "Pioneer One").

* * *

Pioneer One est une série de science-fiction diffusée exclusivement sur Internet. Vous pouvez la visionner en streaming sur [VODO](http://www.vodo.net/ "VODO") (le Jamendo des séries et films), ou bien la télécharger par Bittorrent. Elle compte aujourd’hui six épisodes, et chacun dure 35 à 40 minutes. L’audio est anglais uniquement, mais des [sous-titres](http://www.addic7ed.com/overview/25990/Pioneer%20One "Sous-titres de Pioneer One") réalisés par les fans sont disponibles pour de nombreuses langues.

<iframe src="https://www.kickstarter.com/projects/pioneeronetv/pioneer-one-pilot-episode-for-dramatic-series/widget/video.html" frameborder="0" width="480" height="360"></iframe>

Elle est financée participativement (crowdfounded), en amont de la réalisation des épisodes, via [KickStarter](http://www.kickstarter.com/projects/pioneeronetv/pioneer-one-pilot-episode-for-dramatic-series "Page Kickstarter de Pioneer One"). Le premier épisode a été tourné avec $6000, et à peine plus pour les 3 suivants. Cependant, au total, le projet a réussi à récolter plus de $100000 de dons!

Avec de si petits moyens, les décors sont forcément minimalistes, et les plans sont très souvent fixes. Il y a beaucoup de dialogues, de nature très variée (en face à face, au téléphone, en visio-conférence, etc.). Inconditionnels de l’action et des effets spéciaux s’abstenir!

Cela n’a pas empêché les réalisateurs de construire une ambiance angoissante et intrigante, autour de ce jeune homme maladif tombé des étoiles en combinaison de cosmonaute.

Une série à découvrir.