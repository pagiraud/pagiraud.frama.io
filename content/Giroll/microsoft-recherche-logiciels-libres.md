Title: Radio Giroll : Microsoft et la recherche sur les logiciels libres
Date: 2012-05-18 8:00
Tags: Microsoft, radio giroll, Josh Lerner, Jean Tirole
Slug: microsoft-recherche-logiciels-libres
Il y a 15 jours, le 3 mai 2012, a eu lieu la deuxième édition de [Radio Giroll](http://www.giroll.org/post/2012/04/28/Radio2-jeudi-3-mai-2012 "Radio Giroll #2"). Comme la fois précédente, l’émission a été diffusée sur [Oxyradio](http://www.oxyradio.net/ "OxyRadio, respire et écoute"), et reste disponible en podcast. Cette fois-ci, le menu concocté par <del>l’association</del> le collectif se composait de :

*   [L’analyse de l’accord entre la SACEM et Creative Commons](http://www.giroll.org/post/2012/03/01/L-accord-SACEM-Cr%C3%A9ative-Commons-%3A-une-mauvaise-nouvelle-pour-la-musique-libre. "L'accord SACEM / Creative Commons par Yza") par Yza, qui prend le contre-pied des opinions habituelles, puisqu’elle nous explique qu’il s’agit d’une mauvais nouvelle pour la musique libre.
*   Un point sur [l’évolution institutionnelle de l’ACTA](http://www.giroll.org/post/2012/04/27/Radio2-ACTA%2C-O%C3%B9-en-est-on-%21 "Un point sur l'ACTA") par Yishan, avec un tour d’horizon du point de vue des différents partis et des lois américaines telles SOPA, PIPA et CISPA. Excellent, on peut seulement regretter l’absence du [TPP](https://fr.wikipedia.org/wiki/Trans-Pacific_Strategic_Economic_Partnership "Trans-Pacific Economic Partnership") dans l’analyse.
*   Une [présentation de WebOS](http://www.giroll.org/post/2012/04/27/Radio2-%3A-HP-WebOS-syst%C3%A8me-libre-mobile-en-devenir "WebOS, système libre mobile en devenir"), le système d’exploitation historique de HP, et auparavant de Palm. Il est devenu _open source_ l’année dernière.
*   Quelques exemples de [recherches sur les logiciels libres financées par Microsoft](http://www.giroll.org/post/2012/05/01/Radio2-La-strat%C3%A9gie-globale-de-Microsoft-face-aux-logiciels-libres "Microsoft, quelle stratégie envers le Libre?"), par moi-même.

Comme la dernière fois, les quatre émissions sont très intéressantes, et je vous encourage à toutes les écouter !

 <audio controls>
  <source src="../audio/radio2_microsoft_libre-ep2.ogg" type="audio/ogg">
Your browser does not support the audio element.
</audio> 

Copie de l’article publié sur [le site de Giroll](http://www.giroll.org "Giroll") :

Le ton du discours de Microsoft sur les logiciels libres a bien changé. Si en 2001 Steve Ballmer, actuel CEO (PDG) de Microsoft, a affirmé que Linux était un cancer, en 2007 il a annoncé qu’il aimerait que toute innovation dans l’open source ait lieu sous Windows. Beaucoup y voient un réel infléchissement dans la position du géant de l’informatique sur le sujet. Pourtant, les deux positions ne sont pas contradictoires.

* * *

Le géant mondial de l’informatique a une stratégie globale, cohérente, coordonnée pour atteindre son objectif : maintenir sa situation monopolistique dans un marché en mutation perpétuelle.

Plutôt que de brosser à grands traits un portait abstrait de cette stratégie, nous allons développer dans plusieurs épisodes des actions concrètes de Microsoft, leurs impacts sur le monde du Libre, et leur place dans la stratégie globale de la firme.

# Épisode 2 : Microsoft et le monde de la recherche

En 2010, un ouvrage important sur l’économie du logiciel libre a été publié : _[The Comingled Code](http://www.amazon.fr/The-Comingled-Code-Economic-Development/dp/0262014637/ "The Comingled Code")_ — ce que l’on peut traduire par _Le Code mélangé_. Ses auteurs (Josh Lerner et Mark Schankerman) sont des scientifiques américains reconnus, qui travaillent sur les logiciels libres au moins depuis les années 2000\. Pourtant, certains de leurs résultats sont très surprenants. Ainsi, selon eux, <q>« _policies that would extend the use of open source software will […] decrease social welfare_ »</q> (p.170). (Les politiques visant à intensifier l’utilisation de logiciels libres […] diminuent le bien-être social). Or, les études qui ont conduit à la rédaction du livre ont été financées par Microsoft — ce qui est clairement indiqué en préface (pp x-xi).

![The Comingled Code]({static}/images/comingled_code_cover.jpg "The Comingled Code")]

Microsoft finance donc des recherches en économie sur les logiciels libres. Ces deux chercheurs américains ne sont pas des cas isolés. Par exemple, ils travaillent souvent en tandem avec deux français de la Toulouse School of Economics (TSE), Jean Tirole et Jacques Crémer. La TSE a été [épinglée il y a peu par Laurent Mauduit](http://www.mediapart.fr/journal/economie/070312/lopa-de-la-finance-sur-la-recherche-economique "Laurent Mauduit épingle la TSE dans un ouvrage") à cause de ses importants subsides issus du monde de la finance. Jean Tirole y est même comparé à un virus.

Y a-t-il d’autres chercheurs financés par Microsoft? Quelle est la place de ses financements dans la stratégie globale de Microsoft envers le Libre ?
